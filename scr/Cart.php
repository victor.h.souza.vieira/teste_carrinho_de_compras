<?php
/**
 * Created by PhpStorm.
 * User: victor
 * Date: 24/04/18
 * Time: 19:49
 */

namespace Victor\Cart;
use Victor\Cart\Entities\ProductInterface;

class Cart {
    protected $products;

    /**
     * @return \ArrayObject
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param \ArrayObject $products
     */
    public function addProducts(ProductInterface $product)
    {
        $this->products->append($product);
    }

    public function __construct() {
        $this->products = new \ArrayObject();
    }

    public function getTotal(){
        $total = 0;
        foreach ($this->products as $product) {
            $total+= $product->getPrice();
        }

        return $total;
    }
}